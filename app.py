import sys
import requests
import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# Define the base model
Base = declarative_base()

# Define a sample model class
class User(Base):
    __tablename__ = 'users'
    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    name = sqlalchemy.Column(sqlalchemy.String)

# Create engine
engine = create_engine('sqlite:///example.db')
Base.metadata.create_all(engine)

# Create session
Session = sessionmaker(bind=engine)
session = Session()

# Function to query and order results - vulnerable to SQL Injection
def query_users(order_by):
    # Vulnerable part: order_by should be sanitized or parameterized
    query = "SELECT * FROM users ORDER BY {}".format(order_by)
    result = engine.execute(query)
    for row in result:
        print(row)

# Function to fetch data using requests
def fetch_data(url):
    try:
        response = requests.get(url)
        response.raise_for_status()  # Raises a HTTPError for bad responses
        return response.json()  # Returns the JSON response if successful
    except requests.RequestException as e:
        print(f"Error fetching data: {e}")
        return None

# Main function to run the application
def main():
    if len(sys.argv) != 2:
        print("Usage: python app.py 'COLUMN_NAME'")
        return
    
    order_by = sys.argv[1]
    url = 'https://api.publicapis.org/entries'  # Example public API
    data = fetch_data(url)
    
    if data:
        print(f"Fetched {len(data['entries'])} entries from the API.")

    query_users(order_by)

if __name__ == "__main__":
    main()

